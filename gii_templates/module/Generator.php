<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\gii_templates\module;

use yii\gii\CodeFile;
use yii\helpers\StringHelper;

/**
 * Description of Generator
 *
 * @author Tartharia
 */
class Generator extends \yii\gii\generators\module\Generator{
    public $alias;

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Генератор модулей 1.02';
    }
    
    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'Это измененный генератор модулей. От стандартного отличается тем, что контроллер по умолчанию называется Main. Также добавлен механизм использования файлов перевода модуля';
    }
    
    public function hints() {
        return \yii\helpers\ArrayHelper::merge(parent::hints(),[
            'alias'=>  \Yii::t('system','This is an alias for the namespace of the module. Should look like: <code>vendor_name/module_name</code>.')
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias'], 'filter', 'filter' => 'trim'],
            [['alias'], 'required'],           
            [['alias'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];
        $modulePath = $this->getModulePath();
        $files[] = new CodeFile(
            $modulePath . '/' . StringHelper::basename($this->moduleClass) . '.php',
            $this->render("module.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/controllers/MainController.php',
            $this->render("controller.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/views/main/index.php',
            $this->render("view.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/messages/ru-RU/system.php',
            $this->render("system.php")
        );
        $files[] = new CodeFile(
            $modulePath . '/messages/ru-RU/app.php',
            $this->render("system.php")
        );

        return $files;
    }
    
    /**
     * 
     * @param boolean $relative
     * @return string
     */
    public function getModulePath($relative = false) {
        if($relative)
        {   $start = strpos($this->moduleClass, '\\')+1;
            $path = str_replace('\\', '/', substr($this->moduleClass,$start, strrpos($this->moduleClass, '\\')-$start));
        } else{
            $path = parent::getModulePath();
        }
        return $path;
    }
    
    public function getControllerNamespace() {
        return $this->alias . '\controllers';
    }
}
