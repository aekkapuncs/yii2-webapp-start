<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\module\Generator */

$className = $generator->moduleClass;
$modulePath = str_replace('modules/', '', $generator->getModulePath(true));

$pos = strrpos($className, '\\');
$className = substr($className, $pos + 1);

echo "<?php\n";
?>

namespace <?= $generator->alias ?>;

use Yii;

class <?= $className ?> extends \yii\base\Module
{
    public $defaultRoute = 'main';
    
    public $controllerNamespace = '<?= $generator->getControllerNamespace() ?>';

    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }
    
    protected function registerTranslations()
    {
        Yii::$app->i18n->translations['<?=$modulePath;?>/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/<?=$modulePath;?>/messages',
            'fileMap' => [
                '<?=$modulePath;?>/system' => 'system.php',
                '<?=$modulePath;?>/app' => 'app.php'
            ],
        ];
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('<?=$modulePath;?>/' . $category, $message, $params, $language);
    }
}
