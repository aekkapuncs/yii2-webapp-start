<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes\basic;

/**
 * Description of Theme
 *
 * @author Tartharia
 */
class Theme extends \yii\base\Theme{
    public function init() {
        $this->basePath='@app/themes/basic';
        $this->pathMap=[
            '@app/views' => '@app/themes/basic/views',
            '@eapanel/pages/views'=>'@app/themes/basic/views/yii2-pages',
        ];
    }
}
