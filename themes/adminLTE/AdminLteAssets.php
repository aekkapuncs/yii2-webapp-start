<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes\adminLTE;

/**
 * Description of ThemeAssets
 *
 * @author Tartharia
 */
class AdminLteAssets extends \yii\web\AssetBundle{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/admin-lte/dist';
    
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/skin-blue.min.css',
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'
    ];
    
    public $js = [
        'js/app.min.js'
    ];


    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}