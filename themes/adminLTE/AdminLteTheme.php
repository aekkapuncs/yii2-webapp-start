<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\themes\adminLTE;

/**
 * Description of Theme
 *
 * @author Tartharia
 */
class AdminLteTheme extends \yii\base\Theme{
    public function init() {
        $this->basePath='@app/themes/adminLTE';
        $this->pathMap=[
            '@app/views' => '@app/themes/adminLTE/views',
        ];
    }
}
