<?php

use yii\db\Schema;
use app\migrations\Migration;

class m150325_124502_add_supervisor_user extends Migration
{
    public function safeUp()
    {
        $time = time();
        $this->insert('{{%user}}', [
            'username'=>'robotE13',
            'email' => 'admin@email.ru',
            'password_hash' => '$2y$10$eBi1/MicPbApZ6Xb7N.qPe3sll0.9NLolzxCqEzyFihwA2gr6YwVW',
            'auth_key'=>'QPENWn7wxnmE_8uJWFE50lDsmMJHcGpI',
            'created_at'    => $time,
            'updated_at'    => $time,
            'confirmed_at'  => $time,
        ]);
        $this->insert('{{%profile}}',['user_id'=>1,'gravatar_email'=>'admin@email.ru']);
    }

    public function down()
    {
        echo "m150325_124502_add_supervisor_user cannot be reverted.\n";
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
