<?php

use yii\db\Schema;
use eapanel\publications\migrations\Migration;

class m150223_131136_create_publication_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable("{{%publication}}",[
                'id'=>  Schema::TYPE_PK,
                'created_in'=>  Schema::TYPE_TIMESTAMP . " NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Создано'",
                'updated_in' => Schema::TYPE_TIMESTAMP . " NULL DEFAULT NULL COMMENT 'Обновлено'",
                'author_id' => Schema::TYPE_INTEGER . " NOT NULL COMMENT 'ИД автора публикации'",
                'type' => Schema::TYPE_SMALLINT . " NOT NULL COMMENT 'Тип публикации'",
                'title' => Schema::TYPE_STRING . "(255) NULL DEFAULT NULL COMMENT 'Название публикации'",
                'badge' => Schema::TYPE_STRING . "(128) NULL DEFAULT NULL COMMENT 'Картинка для превью'",
                'preview' => " TINYTEXT NULL DEFAULT NULL COMMENT 'Текст для превью'",
                'body' => Schema::TYPE_TEXT . " NOT NULL COMMENT 'Текст'",
            ], $this->tableOptions);    
        $this->createIndex('fk_publication_user_idx', '{{%publication}}','author_id');
        $this->addForeignKey('fk_publication_user','{{%publication}}','author_id','{{%user}}','id','RESTRICT','CASCADE');
        
        $this->createTable('{{%tag}}',[
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . " NULL DEFAULT NULL COMMENT 'Имя тега'",
            ],  $this->tableOptions);
        
        $this->createTable('{{%publication_tag}}',[
                'publication_id' => Schema::TYPE_INTEGER . ' NOT NULL',
                'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            ], $this->tableOptions);
        $this->addPrimaryKey('', '{{%publication_tag}}',['tag_id','publication_id']);
        $this->createIndex('fk_publication_idx','{{%publication_tag}}', 'publication_id');
        $this->createIndex('fk_tag_idx','{{%publication_tag}}','tag_id');
        $this->addForeignKey('fk_publication','{{%publication_tag}}','publication_id','{{%publication}}','id',"CASCADE", "CASCADE");
        $this->addForeignKey('fk_tag','{{%publication_tag}}','tag_id','{{%tag}}','id',"CASCADE", "CASCADE");
        
        $this->createTable('{{%comment_on_publication}}',[
                'id' => Schema::TYPE_PK,
                'body' => " MEDIUMTEXT NOT NULL COMMENT 'Текст комментария'",
                'created_in' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Создан'",
                'updated_in' => Schema::TYPE_TIMESTAMP . " NULL DEFAULT NULL COMMENT 'Последнее обновление'",
                'publication_id' => Schema::TYPE_INTEGER . " NOT NULL",
                'author_id' => Schema::TYPE_INTEGER . " NOT NULL",
            ], $this->tableOptions);
        
        $this->createIndex('fk_comment_publication_publication_idx', '{{%comment_on_publication}}','publication_id');
        $this->createIndex('fk_comment_publication_user_idx', '{{%comment_on_publication}}','author_id');
        $this->addForeignKey('fk_comment_publication_publication','{{%comment_on_publication}}','publication_id','{{%publication}}','id',"RESTRICT", "CASCADE");
        $this->addForeignKey('fk_comment_publication_user','{{%comment_on_publication}}','author_id','{{%user}}','id',"RESTRICT", "CASCADE");
    }

    public function safeDown()
    {
        $this->dropTable('{{%comment_on_publication}}');        
        $this->dropTable('{{%publication_tag}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%publication}}');
    }
}