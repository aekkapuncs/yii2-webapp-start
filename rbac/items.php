<?php
return [
    'User' => [
        'type' => 1,
        'description' => 'Зарегистрированный пользователь',
    ],
    'Editor' => [
        'type' => 1,
        'description' => 'Редактор',
        'children' => [
            'createNews',
        ],
    ],
    'Administrator' => [
        'type' => 1,
        'description' => 'Администратор сайта',
    ],
    'Supervisor' => [
        'type' => 1,
        'description' => 'Руководитель - суперадминистратор',
        'children' => [
            'Administrator',
            'Editor'
        ],
    ]
];
