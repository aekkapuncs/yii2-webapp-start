<?php
return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=start_template',
            'username' => 'root',
            'password' => '',
        ],
        'mailer' => [
            'useFileTransport' => true,
            'fileTransportPath'=>'@app/mail/dummy'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
