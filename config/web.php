<?php

Yii::setAlias('@app/user', dirname(__DIR__) . '/modules/user');

$config = [
    'id' => 'webapp-start',
    'language'=>'ru-RU',
    'defaultRoute' => 'pages/main/index',
    'components' => [
        'assetManager'=>[
            'forceCopy'=>YII_DEBUG?true:false
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gE3WiADgDVouQdzQtbWHoQvcxRoAqzdU',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\PhpManager',
            'defaultRoles'=>['Guest']
        ],
        'errorHandler' => [
            'errorAction' => 'pages/main/error',
        ],
        'view' => [
            'theme' =>[
                'class'=>'app\themes\basic\Theme',
            ]
        ],
    ],
    'modules' => [
        'pages' => [
            'class' => 'eapanel\pages\Module',
        ],
        'publications' => [
            'class' => 'eapanel\publications\Module',
            'layout' => '@app/themes/adminLTE/views/layouts/main',
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['robotR13'],
            'layout' => '@app/themes/adminLTE/views/layouts/main',
            'controllerMap'=>[
                'security'=>\app\user\SecurityController::className()
            ]
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',     
        'generators' => [ //here
            'ext_module' => [ // generator name
                'class' => 'app\gii_templates\module\Generator', // generator class
                'templates' => [ //setting for out templates
                    'default' => '@app/gii_templates/module/default', // template name => path to template
                ]
            ]
        ],
    ];
}

return $config;