<?php
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . DIRECTORY_SEPARATOR . 'params.php'),
    require (__DIR__ .DIRECTORY_SEPARATOR .'params-loc.php')
);

if(YII_DEBUG)
{
    $targets = [
        [
            'class'=>'yii\log\SyslogTarget',
            'levels'=>['trace']
        ]
    ];
} else {
    $targets = [
        [
            'class'=> 'yii\log\EmailTarget',
            'levels'=>['error','warning'],
            'mailer'=>'mailer',
            'message'=>[
                'from'=>'mail@site.ru',
                'to'=>$params['adminEmail'],
                'subject'=>'Site.ru log'
            ]
        ]
    ];
}

return [
    'basePath' => dirname(__DIR__),
    'aliases'=>[
        '@modules'=>'@app/modules'
    ],
    'bootstrap' => ['log'],
    'language' => 'ru',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\DummyCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport'=>YII_DEBUG,
            'fileTransportPath'=>'@app/mail/dummy'
        ],
        'log' => [
            'class'=>'yii\log\Dispatcher',
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'=>$targets
        ],
        'db'=> [
            'class'=>'\yii\db\Connection',
            'dsn'=>'mysql:host=localhost;dbname=db_name',
            'username'=>'username',
            'password'=>'password',
            'charset'=>'utf8'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [                
                '<_m:[\w\-]+>'=>'<_m>/main/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/<_a>'
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => yii\i18n\PhpMessageSource::className(),
                    'fileMap' => [
                        'app_labels' => 'app_labels.php',
                        'app_messages' => 'app_messages.php',
                        'system' => 'system.php'
                    ],
                ]
            ],
        ],
    ],
    'modules' => [        
    ],
    'params' => $params,
];